# Kivy Example App for the slider widget
import random
import pymsgbox
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from tkinter.filedialog import *
from kivy.uix.floatlayout import FloatLayout
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.slider import Slider
from kivy.uix.label import Label
from kivy.uix.image import Image
import pyperclip as pc
from kivy.uix.button import Button
from kivy.uix.switch import Switch
from kivy.properties  import NumericProperty
from kivy.config import Config
from kivy.uix.boxlayout import BoxLayout
from kivy.config import Config
from kivy.uix.textinput import TextInput
from kivy.uix.checkbox import CheckBox
from kivy.uix.widget import Widget

Config.set('graphics', 'resizable', False)
Config.set('graphics', 'width', '750')
Config.set('graphics', 'height', '600')
Config.set('kivy','window_icon','image_gene/ico.png')

class WidgetContainer(GridLayout):









    def a(self, instance):
        element = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-*/[]#ù%*µ£¤~$%&.:?!=()"

        if self.value_checkbox_let_sym == "non":
            element = element.replace("+-*/[]#ù%*µ£¤~$%&.:?!=()","")

        if self.value_checkbox_let_chif == "non":
            element = element.replace("0123456789","")

        if self.value_checkbox_let_min == "non":
            element = element.replace("abcdefghijklmnopqrstuvwxyz","")

        if self.value_checkbox_let_maj == "non":
            element = element.replace("ABCDEFGHIJKLMNOPQRSTUVWXYZ","")
        self.passwd = ""
        if self.value_checkbox_let_sym == "non" and self.value_checkbox_let_maj == "non" and self.value_checkbox_let_min == "non" and self.value_checkbox_let_chif == "non" :
            pymsgbox.alert("Vous devez au moin cocher une case","CASE")
        else:
            for i in range(int(self.brightnessValue.text)):
                self.passwd = self.passwd + element[random.randint(0, len(element) - 1)]


        if len(self.passwd) < 51 :
            self.mdp_label.font_size = 25

        if len(self.passwd) > 50 and len(self.passwd) <63 :
            self.mdp_label.font_size = 20

        if len(self.passwd) > 63 :
            self.mdp_label.font_size = 17

        self.mdp_label.text = self.passwd



    def copy(self, instance) :
        pc.copy(self.passwd)

    def __init__(self, **kwargs):
        self.value_checkbox_let_min = "non"

        self.value_checkbox_let_maj = "non"

        self.value_checkbox_let_sym = "non"

        self.value_checkbox_let_chif = "non"



        def on_checkbox_active(checkbox, value) :
            if value:
                self.value_checkbox_let_min = "oui"
            else:
                self.value_checkbox_let_min = "non"





        def on_checkbox_active2(checkbox, value) :
            if value:
                self.value_checkbox_let_maj = "oui"
            else:
                self.value_checkbox_let_maj = "non"




        def on_checkbox_active3(checkbox, value) :
            if value:
                self.value_checkbox_let_sym = "oui"
            else:
                self.value_checkbox_let_sym = "non"



        def on_checkbox_active4(checkbox, value) :
            if value:
                self.value_checkbox_let_chif = "oui"
            else:
                self.value_checkbox_let_chif = "non"







        super(WidgetContainer, self).__init__(**kwargs)





        self.btn = Button(text="GÉNÉRER", pos=(315, 190), size=(120, 30))
        self.btn.bind(on_press=self.a)
        self.add_widget(self.btn)



        self.btn_copy = Button(text="Copier", pos=(315, 250), size=(120, 30), background_color=(2.0, 0.0, 0.0, 1.0))
        self.btn_copy.bind(on_press=self.copy)
        self.add_widget(self.btn_copy)



        self.brightnessControl = Slider(min=0, max =70, pos=(125, 70),
                                        value_track_color=[1, 0, 0, 1], size =(500, 20),
                                        value_track=True, size_hint=(100, 100))
        self.add_widget(self.brightnessControl)



        self.checkbox = CheckBox(pos=(80, 450))
        self.checkbox.bind(active=on_checkbox_active)
        self.add_widget(self.checkbox)

        self.checkbox2 = CheckBox(pos=(410, 450))
        self.checkbox2.bind(active=on_checkbox_active2)
        self.add_widget(self.checkbox2)

        self.checkbox3 = CheckBox(pos=(80, 400))
        self.checkbox3.bind(active=on_checkbox_active3)
        self.add_widget(self.checkbox3)

        self.checkbox4 = CheckBox(pos=(410, 400))
        self.checkbox4.bind(active=on_checkbox_active4)
        self.add_widget(self.checkbox4)

        self.lettre_min = Label(text='Lettre minuscule', pos=(170, 450), font_size=17)
        self.add_widget(self.lettre_min)




        self.lettre_min = Label(text='Lettre MAJUSCULES', pos=(510, 450), font_size=17)
        self.add_widget(self.lettre_min)


        self.lettre_min = Label(text='Symbole', pos=(140, 400), font_size=17)
        self.add_widget(self.lettre_min)




        self.lettre_min = Label(text='Chiffre', pos=(460, 400), font_size=17)
        self.add_widget(self.lettre_min)

        self.brightnessValue = Label(text='0', pos=(330, 5), font_size=30)

        self.add_widget(self.brightnessValue)

        self.mdp_label = Label(text='', pos=(330, 100), font_size=25)

        self.add_widget(self.mdp_label)

        # On the slider object Attach a callback for the attribute named value

        self.brightnessControl.bind(value=self.on_value)



    def on_value(self, instance, brightness):
        self.brightnessValue.text = '%d'%brightness












# The app class

class Générateur_de_mot_de_passe(App):

    def build(self):

        widgetContainer = WidgetContainer()

        return widgetContainer



# Run the app

if __name__ == '__main__':

    Générateur_de_mot_de_passe().run()
